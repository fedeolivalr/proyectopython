# ProyectoPython

Painter: Proyecto final de la materia de la materia de programación II de la carrera de Tecnicatura en Programación en UTN La Rioja.

Integrantes:
Federico Oliva(Lider),
Nicolas Parco(Tester),
Leonardo Brizuela(Documentación)

<h2>Uso</h2>
Para ejecutar la aplicación se debe ejecutar desde la clase ventanaPrinicipal<br>
La aplicación cuenta con 4 herramientas: <br>
<ul>
    <li>Pintar</li>
    <li>Borrar</li>
    <li>Linea</li>
    <li>Aerosol</li>
</ul>
Para comenzar a dibujar se debe seleccionar una herramienta y un color y luego comenzar a pintar los cuadros. Tambien cuenta un label en la parte inferior donde se muestran instrucciones.

<h2>Dependencias:</h2>
La aplicación esta hecha con PyQT5 y no cuenta con otras depedencias.