class Paleta: 
    def __init__(self, matriz):
        self.paleta = matriz

    #maneja el string de la hoja de estilo para sacar el valor color Hexadecimal
    def getColor(self,boton):
        i,j = self.posicionDelBoton(boton)
        estilo = self.paleta[i][j].styleSheet()
        indice = estilo.index('#')
        color = estilo[indice:indice+7]
        return color

    #Obtiene la posicion i y j del boton que llega por parametr
    def posicionDelBoton(self,boton):
        for i in range(len(self.paleta)):
            for j in range(len(self.paleta[i])):
                if self.paleta[i][j] == boton:
                    return [i,j]
        return[0,0]