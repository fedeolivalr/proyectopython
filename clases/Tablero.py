class Tablero:
    def __init__(self, tablero):
        self.tablero = tablero

    #Recibe un boton y un color en hexa , lo busca y lo pinta
    def pintarBoton(self,boton,color):
        boton.setStyleSheet(f'background-color: {color}')
    
    def borrarBoton(self,boton):
        self.pintarBoton(boton, '#ffffff')
    #Recibe una posicion y pinta una cruz.
    def aerosolboton(self,boton,color):
        i,j = self.posicionBoton(boton)
        self.pintarBoton(boton,color)
        if not i==0:
            self.pintarBoton(self.tablero[i-1][j],color)
        if not j==19:                
            self.pintarBoton(self.tablero[i][j+1],color)
        if not j==0:
            self.pintarBoton(self.tablero[i][j-1],color)
        if not i==19:
            self.pintarBoton(self.tablero[i+1][j],color)

    def linea(self, boton1, boton2, color):
        i1, j1 = self.posicionBoton(boton1)
        i2, j2 = self.posicionBoton(boton2)
        #Vertical
        if j1 == j2:
            for i in range(*self.limites(i1,i2)):
                self.pintarBoton(self.tablero[i][j1], color)
        #Horizontal
        if i1 == i2:
            for j in range(*self.limites(j1,j2)):
                self.pintarBoton(self.tablero[i1][j], color)
        #Diagonal1
        if (i1-j1) == (i2-j2): 
            j = j1 if j1<j2 else j2
            for i in range(*self.limites(i1,i2)):
                self.pintarBoton(self.tablero[i][j],color)
                j+=1
        #Diagonal2
        if (i1+j1) == (i2+j2):
            j = j2 if j1<j2 else j1
            for i in range(*self.limites(i1,i2)):
                self.pintarBoton(self.tablero[i][j],color)
                j-=1
    #Devuelve la posicion del boton
    def posicionBoton(self,boton):        
        for i in range(len(self.tablero)):
            for j in range(len(self.tablero[i])):
                if self.tablero[i][j] == boton:
                    return [i,j]
        return [0,0]            

    #Invierte los limites para el rango si es necesario
    def limites(self, i1, i2):
        if i1<i2:
            return [i1,i2+1]
        else:
            return [i2, i1+1]