from PyQt5.QtWidgets import QMainWindow, QApplication, QColorDialog, QDialog
from PyQt5 import uic , QtCore
from PyQt5.QtGui import QColor
from Tablero import Tablero
from Paleta import Paleta

class VentanaPrincipal(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("./interfaces/ventanaPrincipal.ui", self)
        self.setFixedSize(1160,645)
        self.color_paleta.clicked.connect(self.onSeleccionarColor)
        self.tablero = Tablero(self.initTablero())
        self.paleta = Paleta(self.initPaleta())
        self.colorSeleccionado = ''
        self.ultimoBotonColor = self.paleta.paleta[0][0]
        self.ultimoBotonTocado = False
        self.herramienta = 'pintar' #va a tener el nombre de la ultima herramienta clickeada
        self.dibujar.clicked.connect(lambda: self.onSelectHerramienta('pintar'))
        self.borrar.clicked.connect(lambda: self.onSelectHerramienta('borrar'))
        self.aerosol.clicked.connect(lambda: self.onSelectHerramienta('aerosol'))
        self.linea.clicked.connect(lambda: self.onSelectHerramienta('linea'))


#Ingresa un nuevo color en la paleta en el ultimo boton seleccionado
    def onSeleccionarColor(self):
        colorObj = QColorDialog.getColor()
        colorHex = colorObj.name()        
        self.ultimoBotonColor.setStyleSheet(f'''background-color: {colorHex};\n
        border: 2px solid #0A0909;\nborder-radius: 10px;''') 
        self.muestraColor.setStyleSheet(f'background-color: {colorHex}')
        self.colorSeleccionado = colorHex   

    def onClickTablero(self, boton):
        #Logica de que herramienta, color y boton usar
        if self.herramienta == 'pintar':
            self.tablero.pintarBoton(boton, self.colorSeleccionado)
        elif self.herramienta == 'borrar':
            self.tablero.borrarBoton(boton)
        elif self.herramienta == 'aerosol':
            self.tablero.aerosolboton(boton,self.colorSeleccionado)
        elif self.herramienta == 'linea' and not(self.ultimoBotonTocado):
            self.ultimoBotonTocado = boton
            self.instrucciones.setText('Toca el siguiente cuadro')
        elif self.herramienta == 'linea' and self.ultimoBotonTocado:  
            self.tablero.linea(self.ultimoBotonTocado, boton, self.colorSeleccionado)
            self.instrucciones.setText('Seleccione el primer cuadro para hacer la linea')
            self.ultimoBotonTocado = False                

    def onSelectHerramienta(self, herramienta):
        if herramienta == 'linea':
            self.instrucciones.setText('Seleccione el primer cuadro para hacer la linea')
        elif herramienta == 'borrar':
            self.instrucciones.setText('Seleccione un cuadro para borrar')
        else:
            self.instrucciones.setText('Seleccione un cuadro para pintar')
        self.herramienta = herramienta
        
#Al tocar un color, lo elije para usarlo y hace los cambios visuales
    def onClickColor(self, boton):
        color = self.paleta.getColor(boton)
        self.colorSeleccionado = color
        self.ultimoBotonColor = boton
        self.muestraColor.setStyleSheet(f'background-color: {color}')          

#Crea una matriz con los botones para iniciar la clase Tablero
    def initTablero(self):
        cols = self.tablero.columnCount()
        rows = self.tablero.rowCount()
        tablero = []
        for i in range(rows):
            row = []
            for j in range(cols):
                item = self.tablero.itemAtPosition(i,j)
                boton = item.widget()
                row.append(boton)
                boton.released.connect(lambda botonActual=boton: self.onClickTablero(botonActual)) 
            tablero.append(row)
        return tablero

#Crea una matriz con los botones para iniciar la clase Paleta
    def initPaleta(self):
        cols = self.paletaColores.columnCount()
        rows = self.paletaColores.rowCount()
        colores = []
        for i in range(rows):
            row = []
            for j in range(cols):
                item = self.paletaColores.itemAtPosition(i,j)
                boton = item.widget()
                row.append(boton)
                boton.released.connect(lambda botonActual=boton: self.onClickColor(botonActual))
            colores.append(row)
        return colores

app = QApplication([])
win = VentanaPrincipal()
win.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowMinimizeButtonHint)
win.show()
app.exec_()